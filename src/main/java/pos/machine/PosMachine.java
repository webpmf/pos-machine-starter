package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        Map<String, Integer> map = createHashMap(barcodes);
        List<ReceiptItem> receiptItems = getReceiptItem(barcodes,map);
        Receipt receipt = caculateTotalPrice(receiptItems);
        String result = generateReceipt(receipt);
        return result;
    }
    public List<Item> loadAllItems() {
        List<Item> items = ItemsLoader.loadAllItems();
        return items;
    }

    public Map<String, Integer> createHashMap(List<String> barcodes){
        Map<String,Integer> map = new HashMap<>();
        for (String barcode : barcodes) {
            if(map.containsKey(barcode)){
                map.put(barcode,map.get(barcode)+1);
            }
            else  {
                map.put(barcode,1);
            }
        }

        return map;
    }
    public List<ReceiptItem> getReceiptItem(List<String> barcodes,Map<String, Integer> map){
        List<ReceiptItem> receiptItems = new ArrayList<>();
        List<Item> items = ItemsLoader.loadAllItems();
        for(String barcode:map.keySet()){
            Item item = findByBarcode(barcode,items);
            ReceiptItem receiptItem = new ReceiptItem(item,map.get(barcode));
            receiptItems.add(receiptItem);
        }
        return receiptItems;
    }





    public Item findByBarcode(String barcodes,List<Item> items){
        for (Item item : items) {
            if(barcodes == item.getBarcode()){
                return item;
            }
        }
        return  null;
    }

    public Receipt caculateTotalPrice(List<ReceiptItem> receiptItems){
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.getUnitPrice();
        }
        return new Receipt(receiptItems,totalPrice);
    }

    public  String generateReceipt(Receipt receipt){
        String receipts = "";
        for (ReceiptItem receiptItem : receipt.getReceiptItems()) {
            receipts += generateItemsReceip(receiptItem)+"\n";
        }
        return renderReceipt(receipts,receipt);
    }

    public String generateItemsReceip(ReceiptItem receiptItem){
        return "Name: "+receiptItem.getItem().getName()+", "+"Quantity: "
                +receiptItem.getQuantity()+", "+"Unit price: "+receiptItem.getItem().getPrice()
                +" (yuan)"+", "+"Subtotal: "+receiptItem.getUnitPrice()+" (yuan)";
    }

    public  String renderReceipt(String receipts,Receipt receipt){

        return "***<store earning no money>Receipt***"+"\n"+receipts+"\n"+"----------------------"+
                "Total: "+receipt.getTotalPrice()+" (yuan)"+"\n"+"**********************";
    }
}
